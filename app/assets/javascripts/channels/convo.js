// App.convo = App.cable.subscriptions.create {channel: "ConvoChannel", room_id: $("#message_room_id").val()},
//   connected: ->
//     # Called when the subscription is ready for use on the server
//     console.log 'connected'+ $("#message_room_id").val()

//   disconnected: ->
//     # Called when the subscription has been terminated by the server

//   received: (data) ->
//     console.log data[]

//   send: (message) ->
//     @perform 'send', message: message\

$( document ).on('turbolinks:load', () => {
	const id = $("#message_room_id").val()
  const current_user_id = $("#current_user_id").val()
  if(id != undefined && current_user_id != undefined){
  	App.convo= App.cable.subscriptions.create(
  	{
  		channel: "ConvoChannel",
  		room_id: id
  	},
  	{
    connected: function() {
    	console.log("connected to room_"+id)
      // Called when the subscription is ready for use on the server
    },

    disconnected: function() {
      // Called when the subscription has been terminated by the server
    },

    received: function(data) {
      if(current_user_id == data['user_id']){
        var check = 'right'
        $("#message_content").val('')
      }
      else{
        var check = 'left'
      }
      $("#chat_bx").append('<li class="message '+ check +' appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">'+data['content']+'</div></div></li>')

      scroll_to_btn()
  	  }
  	});
  }
})

// App.cable.subscriptions.create(
//       {
//         channel: "ConvoChannel",
//         room: 1
//       },
//       {
//         received: function(data) {
//           var content = messageTemplate.children().clone(true, true);
//           content.find('[data-role="user-avatar"]').attr('src', data.user_avatar_url);
//           content.find('[data-role="message-text"]').text(data.message);
//           content.find('[data-role="message-date"]').text(data.updated_at);
//           $element.append(content);
//           $element.animate({ scrollTop: $element.prop("scrollHeight")}, 1000);
//         }
//       }
//     );