module SessionHelper
	def signin(user)
		session[:user_id] = user.id
	end

	def current_user
		User.find_by(id: session[:user_id])
	end

	def authenticate_user
		redirect_to root_path if !current_user.present?
	end

	def signout
		session.delete(:user_id)
	end
end