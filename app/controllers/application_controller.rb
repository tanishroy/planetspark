class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionHelper

  def redirect_to_homepage
  	redirect_to home_path if current_user.present?
  end
end
