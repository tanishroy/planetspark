class SessionController < ApplicationController
	before_action :redirect_to_homepage, only: [:new, :create]
	def new
	end

	def create
		user = User.find_by(username: params[:session][:username])
		if user.present? && user.authenticate(params[:session][:password])
			signin(user)
			redirect_to home_path
		else
			redirect_to root_path
		end
	end

	def destroy
		signout
		redirect_to root_path
	end

end
