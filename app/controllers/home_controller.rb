class HomeController < ApplicationController
	before_action :authenticate_user, except: [:short_url_redirect]

	def index
		@users = User.where.not(id: current_user.id)
		if params[:room_id].present?
			room = Room.find_by(id: params[:room_id])
			if room.present? && room.users.include?(current_user.id.to_s)
				@messages = room.try(:messages).try("order","created_at")
				@users_name = User.where(id: room.users).pluck(:name).join(", ")
			else
				redirect_to home_path
			end
		else
			@urls = current_user.urls
		end
	end

	def urls
	end

	def create_url
		current_user.urls.create(original_url: params[:url_shortner][:url])
		redirect_to home_path
	end

	def short_url_redirect
		url = Url.find_by(short_url_token: params[:url_token]).try(:original_url)
		url = root_path unless url.present?
		redirect_to url
	end

end
