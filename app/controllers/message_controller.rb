class MessageController < ApplicationController

	def create
		@room = Room.find_by(id: params[:message][:room_id])
		@message = Message.create(room_id: params[:message][:room_id], user_id: current_user.id, content: params[:message][:content])
		ConvoChannel.broadcast_to "room_#{@room.id}", @message
	end

	def create_room
		room = Room.select{|x| x.users.include?(current_user.id.to_s) && x.users.include?(params[:user])}.first
		room = Room.create(users: [current_user.id, params[:user].to_i]) if !room.present?
		redirect_to home_path(room_id: room)
	end
end
