class User < ApplicationRecord
	validates :name, presence: true
	validates :username, presence: true, uniqueness: true

	has_many :messages
	has_many :urls
	has_secure_password
end
