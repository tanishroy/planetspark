class Url < ApplicationRecord
	before_create :generate_unique_token
	belongs_to :user

	validates :short_url_token, uniqueness: true

	def generate_unique_token
		self.short_url_token = SecureRandom.hex(4)
		generate_unique_token if self.class.exists?(short_url_token: self.short_url_token)
	end
end
