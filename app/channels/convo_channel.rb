class ConvoChannel < ApplicationCable::Channel
  def subscribed
    stream_for "room_#{params[:room_id]}"
    # stream_from "some_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
