class CreateUrls < ActiveRecord::Migration[5.0]
  def change
    create_table :urls do |t|
      t.integer :user_id
      t.string :original_url
      t.string :short_url_token

      t.timestamps
    end
  end
end
