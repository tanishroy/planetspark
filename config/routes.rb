Rails.application.routes.draw do
	mount ActionCable.server => '/cable'
  resources :users
  root "session#new"

  post '/session', to: "session#create"
  get '/home', to: "home#index"
  delete '/signout', to: "session#destroy"
  post 'msg', to: "message#create"
  post 'room', to: "message#create_room"
  post '/create_url', to: 'home#create_url'
  get '/:url_token', to: "home#short_url_redirect"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
